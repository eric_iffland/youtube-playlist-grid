<?php
require_once 'vendor/autoload.php';

function fetchPlaylistItems($playlistId, $apiKey) {
    $client = new Google_Client();
    $client->setDeveloperKey($apiKey);

    $youtube = new Google_Service_YouTube($client);
    $response = $youtube->playlistItems->listPlaylistItems('snippet', [
        'playlistId' => $playlistId,
        'maxResults' => 50 // Adjust based on your needs
    ]);

    $videos = [];
    foreach ($response['items'] as $item) {
        $videos[] = [
            'title' => $item['snippet']['title'],
            'videoId' => $item['snippet']['resourceId']['videoId']
        ];
    }

    return $videos;
}

$apiKeyFilePath = 'apikey';
$apiKey = file_get_contents($apiKeyFilePath);
$apiKey = trim($apiKey);
$playlistId = 'RD--HpxiARb5I';
$videos = fetchPlaylistItems($playlistId, $apiKey);

echo '<div style="display: grid; grid-template-columns: repeat(auto-fit, minmax(200px, 1fr)); gap: 20px;">';
foreach ($videos as $video) {
    echo sprintf(
        '<div><iframe width="200" height="150" src="https://www.youtube.com/embed/%s" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><p>%s</p></div>',
        $video['videoId'],
        htmlspecialchars($video['title'], ENT_QUOTES, 'UTF-8')
    );
}
echo '</div>';
